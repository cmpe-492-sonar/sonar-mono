## SONAR MONO-REPO ALTERNATIVE

This repo is a monolithic alternative to run all microservices in one place.

TODO, WIP

#### HOW TO RUN

When running for the first time, do not forget to create .env and .env.frontend files, use .env.sample and .env.frontend.sample files as a reference

After creating .env and .env.frontend
```
git submodule update --init --recursive
npm install
npm run update-submodules
npm run build
npm run initialize-semaphores
npm start
```
To run elsevier-fetcher (preferably on another terminal or a separate machine)
```
git submodule update --init --recursive
npm install
npm run update-submodules
npm run build
npm run initialize-semaphores:elsevier
npm run elsevier-fetcher
```


