import Koa from 'koa';
import helmet from 'koa-helmet';
import logger from 'koa-logger';
import respond from 'koa-respond';
import bodyParser from 'koa-bodyparser';
import mount from 'koa-mount';
import staticServer from 'koa-static-server';

import GraphMicroservice from './microservices/graph/src/app';
import ArticleMicroservice from './microservices/article/src/app';
import ElsevierMicroservice from './microservices/elsevier/src/app';
import StitchMicroservice from './microservices/stitch/src/app';

const app = new Koa();

app.use(helmet());

app.use(logger());

app.use(respond({
  statusMethods: {
    tooManyRequests: 429
  }
}));

app.use(bodyParser());

// ERROR HANDLER MIDDLEWARE
app.use(async (ctx, next) => {
  try {
    await next();
  } catch (e) {
    console.log('ERROR', e);
    let { status, message } = e;
    status = status || 500;
    let body = {
      status: status || 500,
      error: message
    };
    if (e.name === 'StatusCodeError') {
      body.error = `${e.name} - ${e.statusCode}`;
    }
    if (e.data || e.error) {
      body.data = e.error;
    }
    ctx.body = body;
    ctx.status = status || 500;
  }
});

app.use(mount('/api/graph', GraphMicroservice));
app.use(mount('/api/elsevier', ElsevierMicroservice));
app.use(mount('/api/article', ArticleMicroservice));
app.use(mount('/api/stitch', StitchMicroservice));

app.use(async (ctx, next) => {
  if (ctx.path.startsWith('/api/')) {
    ctx.notFound('Not Found');
  } else {
    return next();
  }
});

app.use(staticServer({
  rootDir: 'frontend/build',
  rootPath: '/',
  notFoundFile: 'index.html',
  log: false,
}))

app.use(async (ctx, next) => {
  ctx.notFound('Not Found');
});


export default app;

